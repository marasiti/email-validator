package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * @author Timothy Marasigan 991321050
 * 
 * This program checks if a email is valid
 * 
 * It is assumed that spaces should be not be considered as valid characters for the purpose 
 * 
 */

public class EmailValidator {
	
	public static boolean hasOnlyOneAtSymbol(String email) {
		int count = 0;
		
		for(int i = 0; i < email.length(); i++) {
			if(email.charAt(i) == '@') {
				count++;				
			}
		}
		
		if(count == 1) {
			return true;
		}
		
		return false;
	}
	
	public static boolean inCorrectEmailFormat(String email) {
		String regex = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$";
		
		Pattern pattern = Pattern.compile(regex);				
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}
	
	public static boolean accountHasAtLeastThreeAlphaCharsInLowercase(String email) {
		String regex = "^[A-Za-z]{1}.*";	
		
		String[] separated = email.split("@");
		int count = 0;
		
		Pattern pattern = Pattern.compile(regex);				
		Matcher matcher = pattern.matcher(separated[0]);
		
		if(!matcher.matches()) {
			return false;
		}
		
		for(int i = 0; i < separated[0].length(); i++) {
			if(Character.isLowerCase(separated[0].charAt(i))) {
				count++;
			}
		}
		
		if(count >= 3) {
			return true;
		}
		
		return false;
	}
	
	public static boolean domainHasAtLeastThreeAlphaCharsInLowercase(String email) {
		String domain = email.substring(email.indexOf("@"),email.indexOf("."));
		int count = 0;		
		
		for(int i = 0; i < domain.length(); i++) {	
			if(Character.isLowerCase(domain.charAt(i))) {
				count++;
			} else if(Character.isDigit(domain.charAt(i))) {
				count++;
			}					
		}
		
		if(count >= 3) {
			return true;
		}
		
		return false;
	}
	
	public static boolean extensionHasAtLeastTwoAlphaCharacters(String email) {
		String extension = email.substring(email.indexOf(".") + 1);
		int count = 0;
		
		for(int i = 0; i < extension.length(); i++) {
			if(Character.isLowerCase(extension.charAt(i))) {
				count++;
			} else if (Character.isDigit(extension.charAt(i))) {
				return false;
			}
		}
		
		if(count >= 2) {
			return true;
		}
		
		return false;
	}
	

}
