package sheridan;

import static org.junit.Assert.*;
import org.junit.Test;

/*
 * @author Timothy Marasigan 991321050
 * 
 * This program tests the methods in the EmailValidator class.
 * 
 */

public class EmailValidatorTest {
	@Test
	public void testInCorrectEmailFormatRegular() {
		assertTrue("Incorrect email format", EmailValidator.inCorrectEmailFormat("email@email.ca"));
	}
	
	@Test
	public void testInCorrectEmailFormatException() {
		assertFalse("Incorrect email format", EmailValidator.inCorrectEmailFormat("email.ca"));
	}	

	@Test
	public void testInCorrectEmailFormatBoundaryIn() {
		assertTrue("Incorrect email format", EmailValidator.inCorrectEmailFormat("email@email.ca"));
	}
	
	@Test
	public void testInCorrectEmailFormatBoundaryOut() {
		assertFalse("Incorrect email format", EmailValidator.inCorrectEmailFormat("email@@email.ca"));
	}	
	
	@Test
	public void testHasOnlyOneAtSymbolRegular() {		
		assertTrue("Only one @ symbol allowed", EmailValidator.hasOnlyOneAtSymbol("email@email.ca"));		
	}
	
	@Test
	public void testHasOnlyOneAtSymbolException() {		
		assertFalse("Only one @ symbol allowed", EmailValidator.hasOnlyOneAtSymbol("email.ca"));		
	}

	@Test
	public void testHasOnlyOneAtSymbolBoundaryIn() {		
		assertTrue("Only one @ symbol allowed", EmailValidator.hasOnlyOneAtSymbol("a@a.a"));		
	}
	
	@Test
	public void testHasOnlyOneAtSymbolBoundaryOut() {		
		assertFalse("Only one @ symbol allowed", EmailValidator.hasOnlyOneAtSymbol("email@@email.ca"));		
	}
	
	@Test
	public void testAccountHasAtLeastThreeAlphaCharsInLowercaseRegular() {
		assertTrue("Account is in incorrect format", EmailValidator.accountHasAtLeastThreeAlphaCharsInLowercase("email@email.ca"));
	}
	
	@Test
	public void testAccountHasAtLeastThreeAlphaCharsInLowercaseException() {
		assertFalse("Account is in incorrect format", EmailValidator.accountHasAtLeastThreeAlphaCharsInLowercase("EMAIL@email.ca"));
	}
	
	@Test
	public void testAccountHasAtLeastThreeAlphaCharsInLowercaseBoundaryIn() {
		assertTrue("Account is in incorrect format", EmailValidator.accountHasAtLeastThreeAlphaCharsInLowercase("EMail@email.ca"));
	}
	
	@Test
	public void testAccountHasAtLeastThreeAlphaCharsInLowercaseBoundaryOut() {
		assertFalse("Account is in incorrect format", EmailValidator.accountHasAtLeastThreeAlphaCharsInLowercase("EMAil@email.ca"));
	}
	
	@Test
	public void testDomainHasAtLeastThreeAlphaCharsInLowercaseRegular() {
		assertTrue("Domain is in incorrect format", EmailValidator.domainHasAtLeastThreeAlphaCharsInLowercase("email@email.ca"));
	}
	
	@Test
	public void testDomainHasAtLeastThreeAlphaCharsInLowercaseException() {
		assertFalse("Domain is in incorrect format", EmailValidator.domainHasAtLeastThreeAlphaCharsInLowercase("email@E.ca"));
	}
	
	@Test
	public void testDomainHasAtLeastThreeAlphaCharsInLowercaseBoundaryIn() {
		assertTrue("Domain is in incorrect format", EmailValidator.domainHasAtLeastThreeAlphaCharsInLowercase("email@ema.ca"));
	}
	
	@Test
	public void testDomainHasAtLeastThreeAlphaCharsInLowercaseBoundaryOut() {
		assertFalse("Domain is in incorrect format", EmailValidator.domainHasAtLeastThreeAlphaCharsInLowercase("email@ee.ca"));
	}
	
	@Test
	public void testExtensionHasAtLeastTwoAlphaCharsRegular() {
		assertTrue("Extension is in incorrect format", EmailValidator.extensionHasAtLeastTwoAlphaCharacters("email@email.com"));
	}
	
	@Test
	public void testExtensionHasAtLeastTwoAlphaCharsException() {
		assertFalse("Extension is in incorrect format", EmailValidator.extensionHasAtLeastTwoAlphaCharacters("email@email.c1"));
	}
	
	@Test
	public void testExtensionHasAtLeastTwoAlphaCharsBoundaryIn() {
		assertTrue("Extension is in incorrect format", EmailValidator.extensionHasAtLeastTwoAlphaCharacters("email@email.ca"));
	}
	
	@Test
	public void testExtensionHasAtLeastTwoAlphaCharsBoundaryOut() {
		assertFalse("Extension is in incorrect format", EmailValidator.extensionHasAtLeastTwoAlphaCharacters("email@email.c"));
	}
	
	
}








